from django.db import models

# Create your models here.
class Location(models.Model):
    row1 = models.CharField(max_length=150)
    row2 = models.CharField(max_length=150)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=70)
    lat = models.IntegerField(null=True, blank=True)
    lon = models.IntegerField(null=True, blank=True)
    
    def __str__(self):
        return self.row1