""" Serializers of locations """
from rest_framework.serializers import ModelSerializer
from locations.models import Location


# Custom class here
class LocationSerializer(ModelSerializer):
	class Meta:
		model = Location
		fields = [
		    'row1', 'row2', 'city', 'state', 'country', 
		    'lat', 'lon']