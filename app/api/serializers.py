"""
  Serializers of django rest
"""
from rest_framework.serializers import ModelSerializer
from app.models import TuristicPoints

class TuristicPointsSerializer(ModelSerializer):
  class Meta:
    model = TuristicPoints
    fields  = ['id', 'name', 'description']