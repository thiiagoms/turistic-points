"""
 Views of django rest 
"""
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from app.models import TuristicPoints
from .serializers import TuristicPointsSerializer

# Custom viewsets here
class TuristicPointsViewSet(ModelViewSet):
   
    serializer_class = TuristicPointsSerializer

    def get_queryset(self):
        """ overwriting of queryset """
        return TuristicPoints.objects.filter(aproved=True)

    def list(self, request, *args, **kwargs):
        """ Overwriting get request """
        return super(TuristicPointsViewSet, self).list(request, *args, **kwargs)
    
    def create(self, request, *args, **kwargs):
        """ Overwriting post request """
        return super(TuristicPointsViewSet, self).create(request, *args, **kwargs)
    
    def destroy(self, request, *args, **kwargs):
        """ Overwriting delete request """
        return super(TuristicPointsViewSet, self).destroy(request, *args, **kwargs)
    
    def update(self, request, *args, **kwargs):
        """ Overwriting update request """
        return super(TuristicPointsViewSet, self).update(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        """ overwriting partial update method """
        return super(TuristicPointsViewSet, self).partial_update(request, *args, **kwargs)

