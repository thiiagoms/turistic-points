from django.db import models
from attractions.models import Attractions
from comments.models import Comments
from assessments.models import Assessments
from locations.models import Location


# Create your models here.
class TuristicPoints(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    aproved = models.BooleanField(default=False)
    attractions = models.ManyToManyField(Attractions)    
    comments = models.ManyToManyField(Comments)
    aval = models.ManyToManyField(Assessments)
    loc = models.ForeignKey(
        Location, on_delete=models.CASCADE, null=True, blank=True)
    photo = models.ImageField(upload_to='turistic_points', null=True, blank=True) 


    def __str__(self):
        return self.name
