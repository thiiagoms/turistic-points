# django imports
from django.contrib import admin
from django.conf import settings
from django.conf.urls import static
from django.conf.urls import url, include
from django.urls import path
# django rest imports
from rest_framework import routers
# custom imports
from app.api.viewsets import TuristicPointsViewSet
from attractions.api.viewsets import AttractionsViewSet
from locations.api.viewsets import LocationsViewSet
from comments.api.viewsets import CommentsViewSet
from assessments.api.viewsets import AssessmentsViewSet


# custom routes here
router = routers.DefaultRouter()
router.register(r'turistic', TuristicPointsViewSet)
router.register(r'attractions', AttractionsViewSet)
router.register(r'locations', LocationsViewSet)
router.register(r'comments', CommentsViewSet)
router.register(r'assessments', AssessmentsViewSet)

# urls here 
urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
