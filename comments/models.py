from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Comments(models.Model):
    comment_user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.comment_user.first_name
