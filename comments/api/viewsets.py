""" Views of django rest """
from rest_framework.viewsets import ModelViewSet
from comments.models import Comments
from .serializers import CommentsSerializer

class CommentsViewSet(ModelViewSet):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer