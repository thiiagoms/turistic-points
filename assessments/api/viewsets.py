""" Viewsets of django rest """
from rest_framework.viewsets import ModelViewSet
from assessments.models import Assessments
from .serializers import AssessmentsSerializer


class AssessmentsViewSet(ModelViewSet):
    queryset = Assessments.objects.all()
    serializer_class = AssessmentsSerializer