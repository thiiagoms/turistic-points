from rest_framework.serializers import ModelSerializer
from assessments.models import Assessments


class AssessmentsSerializer(ModelSerializer):
    class Meta:
        model = Assessments
        fields = ['id', 'user', 'comments', 'evaluation', 'date']