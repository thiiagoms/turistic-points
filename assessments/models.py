from django.contrib.auth.models import User
from django.db import models
# Create your models here.
class Assessments(models.Model):
    """ Eval model """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comments = models.TextField(null=True, blank=True)
    evaluation = models.DecimalField(max_digits=3, decimal_places=2)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username
